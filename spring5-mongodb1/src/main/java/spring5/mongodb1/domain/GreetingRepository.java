/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring5.mongodb1.domain;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 *
 * @author harry
 */
public interface GreetingRepository extends ReactiveMongoRepository<Greeting, String> {
    
}
