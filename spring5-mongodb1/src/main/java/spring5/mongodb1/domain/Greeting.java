package spring5.mongodb1.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "greetings")
public class Greeting {

    @Id
    private String id;

    @NotBlank
    @Size(max = 200)
    private String content;

//    public Greeting() {
//        this(0, "");
//    }

    public Greeting(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Greeting{" + "id=" + id + ", content=" + content + '}';
    }
    
}
