package spring5.mongodb1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
public class MongoDb1Application {

    public static void main(String[] args) {
        SpringApplication.run(MongoDb1Application.class, args);
    }
}
