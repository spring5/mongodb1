package spring5.mongodb1.web;

import java.util.concurrent.atomic.AtomicLong;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import spring5.mongodb1.domain.Greeting;
import spring5.mongodb1.domain.GreetingRepository;

@RestController
@RequestMapping("/greetings")
public class GreetingController {

    private static final String template = "Hello, %s!";

    @Autowired
    private GreetingRepository greetingRepository;

    @GetMapping("")
    public Flux<Greeting> getAllGreetings() {
        return greetingRepository.findAll();
    }

    @PostMapping("")
    public Mono<Greeting> createGreeting(@Valid @RequestBody Greeting greeting) {
        return greetingRepository.save(greeting);
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Greeting>> updateGreeting(@PathVariable(value = "id") String greetingId,
            @Valid @RequestBody Greeting greeting) {
        return greetingRepository.findById(greetingId)
                .flatMap(g -> {
                    g.setContent(greeting.getContent());
                    return greetingRepository.save(g);
                })
                .map(g -> new ResponseEntity<>(g, HttpStatus.OK))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Greeting>> getGreeting(@PathVariable(value = "id") String greetingId) {
        return greetingRepository.findById(greetingId)
                .map(greeting -> ResponseEntity.ok(greeting))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteGreeting(@PathVariable(value = "id") String greetingId) {
        return greetingRepository.findById(greetingId)
                .flatMap(g -> greetingRepository.delete(g)
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
}
