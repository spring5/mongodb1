package spring5.mongodb1.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoDb1ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoDb1ClientApplication.class, args);
	}
}
