# Getting Started (Gradle)


## Scaffold

    spring init --name=MongoDB1 -g=spring5 -a=mongodb1 -d=webflux,data-mongodb-reactive,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-mongodb1
    spring init --name=MongoDB1Client -g=spring5 -a=mongodb1.client -d=spring-shell,webflux,data-mongodb-reactive,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-mongodb1-client



## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun







